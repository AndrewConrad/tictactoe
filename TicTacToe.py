import copy
import numpy as np

class Tic_Tac_Toe:
    b = np.ones((3, 3), dtype=int)
    __valid_location = 3

    def __init__(self):
        #Instance variables
        self.table = []
        self.x_players_turn = True
        self.winner = False
        self.locations = []
        self.valid_location = True

    # Keeps the window open
    def keep_game_open(self):
        input()

    def draw_game_board(self):
        # Create a 3x3 table that handles the string type
        self.table = np.ndarray((3, 3), dtype=str, )

    def initialize_table(self):
        table = copy.deepcopy(self.table)
        for item in np.nditer(table, op_flags=['readwrite', 'updateifcopy'], flags=['copy_if_overlap']):
            item[...] = '_'
        self.table = table

    def place_marker(self, xlocation, ylocation):
        #Check that location is valid, if not, set valid location to false
        if xlocation > 3 or ylocation > 3:
            self.valid_location = False
        elif xlocation < 0 or ylocation < 0:
            self.valid_location = False
        else:
            self.valid_location = True

        #Let's use traditional coordinates to reference the board
        xlocation = xlocation - 1
        ylocation = ylocation - 1
        if self.x_players_turn == True:
            marker = 'X'
        else:
            marker = 'O'
        if self.valid_location == True:
            self.table[xlocation, ylocation] = marker
            self.x_players_turn = not self.x_players_turn
        if self.valid_location == False:
            print("Please input a valid location")            

    def is_there_winner(self):
        game.winner = False

    def check_if_winner(self, letter):
        _letter = letter
        table = copy.deepcopy(self.table)
        for i in range(3):
            #Checks each row for winner
            if (table[i][0] == _letter and table[i][1] == _letter and table[i][2] == _letter):
                print (_letter, end=" ")
                print ("IS THE WINNER!")
                game.winner = True
            #Checks each column for winner
            if (table[0][i] == _letter and table[1][i] == _letter and table[2][i] == _letter):
                print (_letter, end=" ")
                print ("IS THE WINNER!")
                game.winner = True
        #Check diagonal 1
        if (table[2][0] == _letter and table[1][1] == _letter and table[0][2] == _letter):
            print (_letter, end=" ")
            print ("IS THE WINNER!")
            game.winner = True
        #Check diagonal 2]
        if (table[0][0] == _letter and table[1][1] == _letter and table[2][2] == _letter):
            print (_letter, end=" ")
            print ("IS THE WINNER!")
            game.winner = True
        
    def get_inputs(self):
        print("Input row location")
        xlocation = int(input())
        print("Input column location")
        ylocation = int(input())
        self.locations.insert(0,xlocation)
        self.locations.insert(1,ylocation)
        return self.locations


game = Tic_Tac_Toe()
game.draw_game_board()
game.initialize_table()

while (game.winner == False):
    print(game.table)
    game.get_inputs()
    game.place_marker(game.locations[0], game.locations[1])
    game.check_if_winner("X")
    game.check_if_winner("O")
    game.keep_game_open()

print(game.table)